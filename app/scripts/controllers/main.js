(function(){
	'use strict';
	var app = angular.module('appControllers', [ ]);
	app.controller('MenuCtrl', MenuCtrl);
	app.controller('PageCtrl', PageCtrl);
	app.controller('HomeCtrl', HomeCtrl);
	app.controller('NewsCtrl', NewsCtrl);
	app.controller('ContentsCtrl', ContentsCtrl);
	app.controller('EventsCtrl', EventsCtrl);
	app.controller('ContactsCtrl', ContactsCtrl);
	app.controller('UserCtrl', UserCtrl);
})();