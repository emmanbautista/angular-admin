function MenuCtrl($scope, $routeParams, $http) {
	this.messages = [];
	this.tasks = [];

	this.isActive = function(value){
		
    	if(value == $routeParams.page){
    		return true;
    	}
    };

    this.getMessages = function(){
    	var self = this;
    	$http.get("data/messages.json").success(function(data){
    		self.messages = data;
    	});
    };

    this.getTasks = function(){
    	var self = this;
    	$http.get("data/tasks.json").success(function(data){
    		self.tasks = data;
    	});
    };

    this.getTaskColor = function(value){
    	
    	if(parseInt(value) <= 20){
    		return "progress-bar-danger";
    	}else if(parseInt(value) > 20 && parseInt(value) <= 50){
    		return "progress-bar-warning";
    	}else if(parseInt(value) > 50 && parseInt(value) <= 80){
    		return "progress-bar-info";
    	}else if(parseInt(value) > 80){
    		return "progress-bar-success";
    	}

    	
    };
}