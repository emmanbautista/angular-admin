function EventsCtrl($scope, $http, $element){
	$scope.pageTitle = "Events";
	$scope.data = [];
	$scope.calendar = null;
	$scope.eventItem = null

	$scope.save = function(){
		var d  = new Date();
		console.log($scope.eventItem.startdate);
		var start = $scope.parseDate($scope.eventItem.startdate);
		var end = $scope.parseDate($scope.eventItem.enddate);

		var nEvent = {
			"id" : d.getTime(),
			"title": $scope.eventItem.title,
			"description": $scope.eventItem.description,
			'class': "event-success",
			"start" : start.getTime(),
			"end" : end.getTime(),
		};
		console.log(nEvent);

		$scope.data.push(nEvent);
		$scope.eventItem = {};
		$scope.calendar.setOptions({events_source : $scope.data});
		$scope.calendar.view();
		$("#eventEditor").modal('hide');
	};

	$scope.addEvent = function(){
		$scope.eventItem = {};
		$("#eventEditor").modal('show');
		/*var d  = new Date();
		$scope.data.push({
     		"id": d.getTime(),
	      	"title": "This is warning class event with very long title to check how it fits to evet in day view",
	      	"url": "http://www.example.com/",
	      	"class": "event-warning",
	      	"start":  d.getTime(),
			"end":    d.getTime() + 200
		});
		$scope.calendar.setOptions({events_source : $scope.data});
	
		$scope.calendar.view();*/


	};

	$scope.initCalendar = function($e){
		$http.get("data/events.json").success(function(data){
			$scope.data = data.result;
			$scope.calendar = $e.calendar({
		        tmpl_path: "bower_components/bootstrap-calendar/tmpls/",
		        day: '2013-03-12',
		        view: 'month',
		        tmpl_cache: false,
		        events_source: $scope.data,
		        onAfterEventsLoad: function(events) {
					if(!events) {
						return;
					}
					
					var list = $('.cal-list-event');
					list.empty();

					$.each(events, function(key, val) {
						$(document.createElement('li'))
							.html('<a href="' + val.url + '">' + val.title + '</a>')
							.appendTo(list);
					});


				},
		        onAfterViewLoad: function(view) {
					$('.page-header h3, .event-list .panel-heading .today').text(this.getTitle());
					$('.btn-group button').removeClass('active');
					$('button[data-calendar-view="' + view + '"]').addClass('active');
				},
				classes: {
					months: {
						general: 'label'
					}
				}
		    });
	    });
	}

	$scope.parseDate = function(str){
		var dateParts = str.split("-");
	    if (dateParts.length != 3)
	        return null;
	    var year = dateParts[0];
	    var month = dateParts[1];
	    var day = dateParts[2];

	    if (isNaN(day) || isNaN(month) || isNaN(year))
	        return null;

	    var result = new Date(year, (month - 1), day);
	    if (result == null)
	        return null;
	    if (result.getDate() != day)
	        return null;
	    if (result.getMonth() != (month - 1))
	        return null;
	    if (result.getFullYear() != year)
	        return null;

	    return result;
	}
};