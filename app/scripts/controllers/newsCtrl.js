function NewsCtrl($scope, $routeParams, $http) {
	$scope.pageTitle = "News";
	$scope.data =  [];
	$scope.content = {};

	$scope.getContents = function(){
		$http.get("data/news.json").success(function(data){
			$scope.data = data;
		});
	};

	$scope.delete = function(id){
		var y = confirm("Are you sure you want to delete $scope content? ");
		if(y){
			$($scope.data).each(function(i, e){
				if(e.id == id){
					$scope.data.splice(i, 1);
				}
			});
		}
	};

	$scope.edit = function(id){
		$($scope.data).each(function(i, e){
			if(e.id == id){
				$scope.content  = e;
				$("#contentEditor").modal('show');
			}
		});
	};

	$scope.publish = function(id){
		$($scope.data).each(function(i, e){
			if(e.id == id){
				$scope.data[i].published= !e.published;
			}
		});
	};

	$scope.save = function(){
		if($scope.content.id == undefined) { //add
			var d = new Date();
			$scope.content.createdOn = d.getMonth() + "/" + d.getDate() + "/" + d.getFullYear();
			$scope.content.id = d.getTime();
			$scope.data.push($scope.content);
			$scope.content = {};
		}else{ //edit

		}
		$("#contentEditor").modal('hide');
	};

	$scope.add = function(){
		$scope.content = {};
		$("#contentEditor").modal('show');
	}
}