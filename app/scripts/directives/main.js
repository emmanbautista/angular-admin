(function(){
	var app = angular.module("appHelpers", [ ]);
	app.directive("appMenu", function(){
		return {
			"restrict": "E",
			"templateUrl" : "views/menu.html",
			"controller" : "MenuCtrl",
			"controllerAs" : "menu",
			"link": function($scope, $element){
				$element.find("#side-menu").metisMenu();
			}
		};
	});

	app.directive("appPageTitle", function(){
		return {
			"restrict": "A",
			"templateUrl" : "views/viewTitle.html",
		};
	});

	app.directive("appContentItem", function(){
		return {
			"restrict": "A",
			"templateUrl" : "views/cms/item.html",
		};
	});

	app.directive("adminCalendar", function($http){
		return {
			"restrict": "E",
			"template" : "<div></div>",
			"link": function postLink(scope, element, attrs) {
				scope.initCalendar(element);

		      	
		        $('.btn-group button[data-calendar-nav]').each(function() {
					var $this = $(this);
					$this.click(function() {
						scope.calendar.navigate($this.data('calendar-nav'));
					});
				});

				$('.btn-group button[data-calendar-view]').each(function() {
					var $this = $(this);
					$this.click(function() {
						scope.calendar.view($this.data('calendar-view'));
					});
				});

				$('#first_day').change(function(){
					var value = $(this).val();
					value = value.length ? parseInt(value) : null;
					scope.calendar.setOptions({first_day: value});
					scope.calendar.view();
				});

				$('#language').change(function(){
					scope.calendar.setLanguage($(this).val());
					scope.calendar.view();
				});

				$('#events-in-modal').change(function(){
					var val = $(this).is(':checked') ? $(this).val() : null;
					scope.calendar.setOptions({modal: val});
				});
				$('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
					//e.preventDefault();
					//e.stopPropagation();
				});
			    
		    }
		};
	});

})();