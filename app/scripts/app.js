(function(){
  'use strict';

  var app = angular
    .module('apptestApp', [
      'ngCookies',
      'ngResource',
      'ngSanitize',
      'ngRoute',
      'appHelpers',
      'appControllers'
    ]);
  
  app.config(function ($routeProvider) {
      $routeProvider
        .when('/:page', {
          templateUrl: function(params){
            return 'views/' + params.page + ".html";
          }
        })
        .when('/:page/:sub', {
          templateUrl: function(params){
             return "views/" +params.page + "/" + params.sub + ".html";
          }
        })
        .when('/:page/:sub/:action/:id', {
          templateUrl: function(params){
             return "views/" +params.page + "/" +  params.action + ".html";
          }
        })
        .otherwise({
          redirectTo: '/home'
        });
    });
  })();

